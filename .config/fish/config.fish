if status is-interactive
    # Commands to run in interactive sessions can go here
end

### FISH PROMPT ###
set -U fish_greeting
#function fish_prompt
#    set_color $fish_color_cwd
#    set_color cyan
#    echo -n (prompt_pwd)
#    set_color normal
#    echo -n ' ❯ '
#end
starship init fish | source

### STARTX AT LOGIN ###
if status --is-login
  if test -z "$DISPLAY" -a $XDG_VTNR = 1
    exec startx -- -keeptty
  end
end

### SEARCH FZF ###
function s
du -a | awk '{print $2}' | fzf | xargs -r nvim
end

### RANDOM COLOR SCRIPT ###
colorscript random

### DEFAULTS ###
export EDITOR=nvim
export VISUAL=nvim
export TERMINAL=st
export BROWSER=firefox

### ALIASES ###
alias fsh="nvim ~/.config/fish/config.fish"
alias bsh="nvim ~/.bashrc"
alias x="nvim ~/.xinitrc"
alias i="doas pacman -S"
alias r="doas pacman -Rns"
alias v="nvim"
alias n="nnn"
alias q="cd .."
alias qq="cd ../.."
alias ls="exa"
alias la="exa -la"
alias ll="exa -l"
alias c="bat"
alias pic="nvim ~/.config/picom/picom.conf"
alias dwm="cd ~/graffiti/dwm && nvim config.h"
alias stc="cd ~/graffiti/st && nvim config.h"
alias slstatus="cd ~/graffiti/slstatus && nvim config.h"
alias dmenu="cd ~/graffiti/dmenu && nvim config.h"
alias w="nmtui"
alias clean="doas make clean install"
alias save="cp config.h config.def.h && doas make clean install"

############################
### REBUILDING FUNCTIONS ###
############################

function rebuild-dwm
cd
mv ~/graffiti ~/graffiti2
git clone https://gitlab.com/ubrrr/graffiti.git
doas rm -fr ~/graffiti2/dwm
mv ~/graffiti/dwm ~/graffiti2/.
doas rm -fr ~/graffiti
mv ~/graffiti2 ~/graffiti
cd ~/graffiti/dwm
doas make clean install
cd
end

function rebuild-st
cd
mv ~/graffiti ~/graffiti2
git clone https://gitlab.com/ubrrr/graffiti.git
doas rm -fr ~/graffiti2/st
mv ~/graffiti/st ~/graffiti2/.
doas rm -fr ~/graffiti
mv ~/graffiti2 ~/graffiti
cd ~/graffiti/st
doas make clean install
cd
end

function rebuild-slstatus
cd
mv ~/graffiti ~/graffiti2
git clone https://gitlab.com/ubrrr/graffiti.git
doas rm -fr ~/graffiti2/slstatus
mv ~/graffiti/slstatus ~/graffiti2/.
doas rm -fr ~/graffiti
mv ~/graffiti2 ~/graffiti
cd ~/graffiti/slstatus
doas make clean install
cd
end

function rebuild-dmenu
cd
mv ~/graffiti ~/graffiti2
git clone https://gitlab.com/ubrrr/graffiti.git
doas rm -fr ~/graffiti2/dmenu
mv ~/graffiti/dmenu ~/graffiti2/.
doas rm -fr ~/graffiti
mv ~/graffiti2 ~/graffiti
cd ~/graffiti/dmenu
doas make clean install
cd
end
