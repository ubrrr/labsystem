"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Theming
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
highlight Normal           guifg=#dfdfdf ctermfg=white    guibg=#282c34 ctermbg=none  cterm=none
highlight CursorLineNr     guifg=#202328 ctermfg=white    guifg=#5b6268 ctermbg=none  cterm=none
highlight StatusLine       guifg=#202328 ctermfg=7   	  guifg=#5b6268 ctermbg=none  cterm=none

nnoremap S :%s//g<left><left>
set clipboard+=unnamedplus
set mouse=nicr
set mouse=a
set number relativenumber
