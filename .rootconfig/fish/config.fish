if status is-interactive
    # Commands to run in interactive sessions can go here
end

set -U fish_greeting
starship init fish | source

### SEARCH ###
function s
du -a / | awk '{print $2}' | dmenu -l 15 | xargs -r nvim
end

### RANDOM COLOR SCRIPT ###
#colorscript random

### DEFAULTS ###
export EDITOR=nvim
export VISUAL=nvim
export TERMINAL=st
export BROWSER=firefox

### ALIASES ###
alias fsh="nvim ~/.config/fish/config.fish"
alias bsh="nvim ~/.bashrc"
alias i="pacman -S"
alias r="pacman -Rns"
alias v="nvim"
alias n="nnn"
alias q="cd .."
alias qq="cd ../.."
alias ls="exa"
alias la="exa -la"
alias ll="exa -l"
alias c="bat"
alias w="nmtui"
alias clean="make clean install"
